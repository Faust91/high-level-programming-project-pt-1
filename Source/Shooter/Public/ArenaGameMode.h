// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include <Chaos/Array.h>
#include "ArenaGameMode.generated.h"

class AArenaEnemy;

/**
 *
 */
UCLASS()
class SHOOTER_API AArenaGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AArenaGameMode();

	void PawnKilled(APawn* PawnKilled);

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<AArenaEnemy> EnemyClass;

protected:
	virtual void BeginPlay() override;

private:
	void SpawnWave();
	void EndGame(bool bIsPlayerWinner);

	int32 maxEnemies = 10;
	int32 numEnemies = 0;
	TArray<AActor*> spawnPoints;

};
