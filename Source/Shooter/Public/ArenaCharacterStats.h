// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ArenaCharacterStats.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SHOOTER_API UArenaCharacterStats : public UActorComponent
{
	GENERATED_BODY()

public:
	UArenaCharacterStats();

	void UpdateStat(const FString& StatName, float StatValue);

protected:

	UFUNCTION()
		float BulletsEnemiesRatio();

	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0", UMin = "0"))
		int FiredBullets;
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0", UMin = "0"))
		int KilledEnemies;
	UPROPERTY(EditAnywhere, Category = "Stats", meta = (ClampMin = "0", UMin = "0"))
		int PickedItems;

};
