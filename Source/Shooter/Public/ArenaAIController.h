// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ArenaAIController.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API AArenaAIController : public AAIController
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintNativeEvent)
		void ChasePlayer();

	bool IsDead() const;

protected:

	virtual void BeginPlay() override;
};
