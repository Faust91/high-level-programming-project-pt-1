// Fill out your copyright notice in the Description page of Project Settings.

#include "ArenaWeapon.h"
#include "ArenaProjectile.h"
#include <Components/SkeletalMeshComponent.h>
#include <Components/ArrowComponent.h>
#include "ArenaItemDataAsset.h"
#include "ArenaCharacter.h"
#include "ArenaCharacterInventory.h"

// Sets default values
AArenaWeapon::AArenaWeapon()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	skeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(FName(TEXT("SkeletalMesh")));
	RootComponent = skeletalMesh;
	skeletalMesh->SetCollisionProfileName(TEXT("NoCollision"));
	// TODO: clothing simulation factory

	ConstructorHelpers::FObjectFinder<USkeletalMesh> meshAsset(TEXT("/Game/FPWeapon/Mesh/SK_FPGun"));
	if (meshAsset.Succeeded()) {
		mesh = meshAsset.Object;
		skeletalMesh->SetSkeletalMesh(mesh);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Mesh not found!"))
	}

	ConstructorHelpers::FObjectFinder<UMaterialInstance> materialAsset(TEXT("/Game/FPWeapon/Materials/MI_FPGun"));
	if (materialAsset.Succeeded()) {
		materialInstance = materialAsset.Object;
		skeletalMesh->SetMaterial(0, materialInstance);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Material instance not found!"))
	}

	arrow = CreateDefaultSubobject<UArrowComponent>(FName(TEXT("Gun")));
	arrow->AttachToComponent(skeletalMesh, FAttachmentTransformRules::KeepRelativeTransform);
	arrow->SetRelativeLocation(FVector(0.0f, 50.0f, 10.0f));
	arrow->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));
	arrow->ArrowSize = 0.5f;

}

// Called when the game starts or when spawned
void AArenaWeapon::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AArenaWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AArenaWeapon::StartFire_Implementation() {
	if (bDoOnce)
	{
		Fire();
		GetWorld()->GetTimerManager().SetTimer(fireTimer, this, &AArenaWeapon::Fire, fireRate, true, fireDelay);
		bDoOnce = false;
	}
}

void AArenaWeapon::StopFire_Implementation() {
	GetWorld()->GetTimerManager().ClearTimer(fireTimer);
	fireTimer.Invalidate();
	bDoOnce = true;
}

void AArenaWeapon::Fire_Implementation() {
	AArenaProjectile* projectile = GetWorld()->SpawnActor<AArenaProjectile>(ProjectileClass.Get(), arrow->GetComponentLocation(), arrow->GetComponentRotation());
	projectile->SetOwner(this->GetOwner());
	SendItemData(FireItemData);
}

void AArenaWeapon::SendItemData(UArenaItemDataAsset* i_itemData)
{
	AArenaCharacter* arenaCharacter = Cast<AArenaCharacter>(GetOwner());
	if (arenaCharacter) {
		UArenaCharacterInventory* inventory = arenaCharacter->FindComponentByClass<UArenaCharacterInventory>();
		if (inventory) {
			inventory->AddItemData(i_itemData);
		}
	}
}