// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaCharacterStats.h"
#include "ReflectionUtilsFunctionLibrary.h"
#include "Engine.h"

// Sets default values for this component's properties
UArenaCharacterStats::UArenaCharacterStats() :
	FiredBullets{ 0 }, KilledEnemies{ 0 }, PickedItems{ 0 }
{
	PrimaryComponentTick.bCanEverTick = false;

}

void UArenaCharacterStats::UpdateStat(const FString& StatName, float StatValue)
{

	/*
	*	Example:
	*		StatName: "HP"
	*		InPath: "This." + StatName
	*/

	FString Path = "This";
	Path += ".";
	Path += StatName;

	void* OutObject = nullptr;
	FProperty* StatProperty = UReflectionUtilsFunctionLibrary::RetrieveProperty(this, Path, OutObject);

	if (StatProperty) {
		FFloatProperty* FloatProperty = CastField<FFloatProperty>(StatProperty);
		if (FloatProperty) {
			float Value = StatValue;
			float OldValue = FloatProperty->GetPropertyValue_InContainer(this);
			FloatProperty->SetPropertyValue_InContainer(OutObject, OldValue + Value);
		}
		else {
			FIntProperty* IntProperty = CastField<FIntProperty>(StatProperty);
			if (IntProperty) {
				float Value = StatValue;
				float OldValue = IntProperty->GetPropertyValue_InContainer(this);
				IntProperty->SetPropertyValue_InContainer(OutObject, OldValue + Value);
			}
		}
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Stats update"));
		}
	}
}

float UArenaCharacterStats::BulletsEnemiesRatio()
{
	return KilledEnemies > 0 ? (float)FiredBullets / (float)KilledEnemies : -1.0f;
}
