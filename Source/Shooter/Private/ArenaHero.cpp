// Fill out your copyright notice in the Description page of Project Settings.

#include "ArenaHero.h"
#include "ArenaWeapon.h"
#include <GameFramework/SpringArmComponent.h>
#include <Camera/CameraComponent.h>
#include "Kismet/KismetMathLibrary.h"
#include <Kismet/GameplayStatics.h>
#include "ArenaCharacterTelemetryComponent.h"
#include <DisplayDebugHelpers.h>

AArenaHero::AArenaHero() {

	springArm = CreateDefaultSubobject<USpringArmComponent>(FName(TEXT("SpringArm")));
	springArm->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	springArm->SetRelativeRotation(FRotator(-90.0f, 0.0f, 0.0f));
	springArm->TargetArmLength = 1000.0f;
	springArm->bDoCollisionTest = false;
	springArm->bInheritPitch = false;
	springArm->bInheritYaw = false;
	springArm->bInheritRoll = false;

	camera = CreateDefaultSubobject<UCameraComponent>(FName(TEXT("Camera")));
	camera->AttachToComponent(springArm, FAttachmentTransformRules::KeepRelativeTransform);

	telemetry = CreateDefaultSubobject<UArenaCharacterTelemetryComponent>(FName(TEXT("Telemetry")));

}

// Called when the game starts or when spawned
void AArenaHero::BeginPlay()
{
	Super::BeginPlay();
}

void AArenaHero::StartFire() {
	weapon->StartFire();
}

void AArenaHero::StopFire() {
	weapon->StopFire();
}

void AArenaHero::DisplayDebug(UCanvas* Canvas, const FDebugDisplayInfo& DebugDisplay, float& YL, float& YPos)
{
	Super::DisplayDebug(Canvas, DebugDisplay, YL, YPos);

	static FName NAME_Telemetry = FName(TEXT("TELEMETRY"));

	if (DebugDisplay.IsDisplayOn(NAME_Telemetry))
	{
		telemetry->DrawTelemetry(Canvas, YL, YPos);
	}
}
