// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"
#include "ArenaGameMode.h"
#include "ArenaCharacter.h"
#include <Kismet/GameplayStatics.h>
#include "Components/CapsuleComponent.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	Health = DefaultHealt;
	GameModeRef = Cast<AArenaGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::TakeDamage);
}

void UHealthComponent::TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (IsDead() || Damage <= 0.0f) {
		return;
	}

	Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealt);
	if (IsDead()) {
		if (GameModeRef) {
			GameModeRef->PawnKilled(Cast<AArenaCharacter>(GetOwner()));
		}
		Cast<AArenaCharacter>(GetOwner())->DetachFromControllerPendingDestroy();
		Cast<AArenaCharacter>(GetOwner())->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

bool UHealthComponent::IsDead() const
{
	return Health <= 0.0f;
}

