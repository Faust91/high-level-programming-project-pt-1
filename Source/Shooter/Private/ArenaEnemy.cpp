// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaEnemy.h"
#include <Kismet/GameplayStatics.h>
#include <AIController.h>
#include "ArenaAIController.h"
#include <GameFramework/CharacterMovementComponent.h>


AArenaEnemy::AArenaEnemy() {

	AIControllerClass = AArenaAIController::StaticClass();

	GetCharacterMovement()->MaxWalkSpeed = 300.0f;

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

}

// Called when the game starts or when spawned
void AArenaEnemy::BeginPlay()
{
	Super::BeginPlay();
}

void AArenaEnemy::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	UMaterial* materialEnemy = LoadObject<UMaterial>(nullptr, TEXT("/Game/Materials/Mat_Enemy"));
	if (materialEnemy) {
		GetMesh()->CreateDynamicMaterialInstance(0, materialEnemy);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Material not found!"))
	}

}
