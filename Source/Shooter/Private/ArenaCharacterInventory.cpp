
#include "ArenaCharacterInventory.h"
#include "ArenaCharacterStats.h"
#include "ArenaItem.h"
#include "ArenaLogContext.h"
#include "ArenaItemDataAsset.h"
#include <Engine/DataTable.h>
#include "ArenaItemStatModifier.h"

UArenaCharacterInventory::UArenaCharacterInventory()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
}

void UArenaCharacterInventory::InitializeComponent()
{
	Super::InitializeComponent();

	AActor* Owner = GetOwner();
	if (Owner) {
		CharacterStats = Owner->FindComponentByClass<UArenaCharacterStats>();
	}
}

void UArenaCharacterInventory::UninitializeComponent()
{
	Super::UninitializeComponent();

	CharacterStats = nullptr;
}

void UArenaCharacterInventory::AddItem(AArenaItem* i_Item)
{
	if (!ensureAlways(i_Item)) {
		UE_LOG(LogArena, Warning, TEXT("Invalid Item"));
		return;
	}
	AddItemData(i_Item->ItemData);
}

void UArenaCharacterInventory::AddItemData(UArenaItemDataAsset* i_ItemData)
{
	if (!ensure(CharacterStats)) {
		UE_LOG(LogArena, Warning, TEXT("Missing Character Stats"));
		return;
	}

	if (!i_ItemData) {
		UE_LOG(LogArena, Warning, TEXT("Invalid Item DataAsset"));
		return;
	}

	UE_LOG(LogArena, Log, TEXT("Added Item %s"), *i_ItemData->ItemName.ToString());

	UDataTable* Modifiers = i_ItemData->StatModifiers;

	if (!Modifiers) {
		UE_LOG(LogArena, Warning, TEXT("Invalid Item DataTable"));
		return;
	}

	static const FString ContextString(TEXT("UArenaCharacterInventory::AddItem"));

	TArray<FArenaItemStatModifier*> Rows;
	Modifiers->GetAllRows(ContextString, Rows);

	int NumRows = Rows.Num();

	for (size_t RowIndex = 0; RowIndex < NumRows; ++RowIndex) {
		FArenaItemStatModifier* Row = Rows[RowIndex];
		CharacterStats->UpdateStat(Row->StatName, Row->Value);
	}
}
