// Fill out your copyright notice in the Description page of Project Settings.


#include "ArenaCharacter.h"
#include "ArenaWeapon.h"
#include "ArenaGameMode.h"
#include "HealthComponent.h"
#include "ArenaCharacterStats.h"
#include "ArenaItem.h"
#include "ArenaCharacterInventory.h"

AArenaCharacter::AArenaCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, -90.0f));
	GetMesh()->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	ConstructorHelpers::FObjectFinder<USkeletalMesh> meshAsset(TEXT("/Game/AnimationAssets/UE4_Mannequin/Mesh/SK_Mannequin"));
	if (meshAsset.Succeeded()) {
		GetMesh()->SetSkeletalMesh(meshAsset.Object);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Mesh not found!"))
	}

	ConstructorHelpers::FClassFinder<UAnimInstance> animationAssetClass(TEXT("/Game/AnimationAssets/GameBaseCharacterAnimBP"));
	if (animationAssetClass.Succeeded()) {
		GetMesh()->SetAnimClass(animationAssetClass.Class);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Animation class not found!"))
	}

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));

	StatsComponent = CreateDefaultSubobject<UArenaCharacterStats>(TEXT("Stats"));
	InventoryComponent = CreateDefaultSubobject<UArenaCharacterInventory>(TEXT("Inventory"));
}

void AArenaCharacter::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (!OtherActor || OtherActor->IsPendingKill()) {
		return;
	}
	bool bIsAnItem = OtherActor->IsA<AArenaItem>();
	if (bIsAnItem) {
		AArenaItem* Item = Cast<AArenaItem>(OtherActor);
		InventoryComponent->AddItem(Item);
		Item->Destroy();
	}
}

bool AArenaCharacter::IsDead() const
{
	return HealthComponent->IsDead();
}

void AArenaCharacter::BeginPlay()
{
	Super::BeginPlay();

	weapon = GetWorld()->SpawnActor<AArenaWeapon>(WeaponClass.Get(), GetMesh()->GetComponentTransform().GetLocation(), GetMesh()->GetComponentTransform().GetRotation().Rotator());
	FAttachmentTransformRules weaponRules = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false);
	weapon->AttachToComponent(GetMesh(), weaponRules, FName(TEXT("GunSocket")));
	weapon->SetOwner(this);

}

void AArenaCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AArenaCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AArenaCharacter::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

}

void AArenaCharacter::DisplayDebug(UCanvas* Canvas, const FDebugDisplayInfo& DebugDisplay, float& YL, float& YPos)
{
	Super::DisplayDebug(Canvas, DebugDisplay, YL, YPos);
}
