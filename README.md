To test all the features enter these 2 lines in your command line:

ShowDebug TELEMETRY

ArenaCharacterTelemetry.List Stats.PickedItems,Stats.FiredBullets,Stats.KilledEnemies,Stats.BulletsEnemiesRatio

Stats.PickedItems, Stats.FiredBullets and Stats.KilledEnemies get their values from FProperty objects.
Stats.BulletsEnemiesRatio retrieves its value from a return value of a UFunction.

This telemetry works only with float properties, int properties and functions that return float.
