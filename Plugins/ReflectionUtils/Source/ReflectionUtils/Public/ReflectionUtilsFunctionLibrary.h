// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ReflectionUtilsFunctionLibrary.generated.h"

UCLASS()
class REFLECTIONUTILS_API UReflectionUtilsFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/*
	*	Examples:
	*		This.CharacterStats.HP (This: AArenaCharacter)
	*		This.CharacterStats.MP (This: AArenaCharacter)
	*		This.CharacterMovement.Speed (This: AArenaCharacter)
	*		This.HP (This: UArenaCharacterStats)
	*/
	static FProperty* RetrieveProperty(UObject* InObject, const FString& InPath, void*& OutTarget);
	static UFunction* RetrieveFunction(UObject* InObject, const FString& InPath, void*& OutTarget);

};
