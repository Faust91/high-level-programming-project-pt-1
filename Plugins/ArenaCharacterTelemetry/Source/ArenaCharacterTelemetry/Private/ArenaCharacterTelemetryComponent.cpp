// Fill out your copyright notice in the Description page of Project Settings.

#include "ArenaCharacterTelemetryComponent.h"

#include "Engine/Canvas.h"
#include "Engine/Font.h"
#include "RenderUtils.h"
#include "ReflectionUtilsFunctionLibrary.h"
#include <GenericPlatform/GenericPlatformMemory.h>

#pragma optimize("", off)

namespace {
	void LocDrawText(UCanvas* InCanvas, const FString& InLabel, const FColor& InColor, float InX, float InY, float InMaxWidth, float& OutX, float& OutY)
	{
		UFont* Font = GEngine->GetSmallFont();

		InCanvas->SetDrawColor(InColor);

		float XL, YL;
		InCanvas->TextSize(Font, InLabel, XL, YL);

		float ScaleX = 1.f;
		if (XL > InMaxWidth)
		{
			ScaleX = InMaxWidth / XL;
		}

		InCanvas->DrawText(Font, InLabel, InX, InY, ScaleX);

		OutX = InX + XL;
		OutY = InY + YL;
	}

} // namespace

static TAutoConsoleVariable<FString> CVarArenaCharacterTelemetryList(
	TEXT("ArenaCharacterTelemetry.List"),
	TEXT(""),
	TEXT("List of telemetry queries."),
	ECVF_Default);

UArenaCharacterTelemetryComponent::UArenaCharacterTelemetryComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UArenaCharacterTelemetryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateTelemetryTargets();
}

void UArenaCharacterTelemetryComponent::UpdateTelemetryTargets()
{
	FString TelemetryList = CVarArenaCharacterTelemetryList.GetValueOnGameThread();

	if (CurrentTelemetryList.Compare(TelemetryList) != 0)
	{
		TelemetryTargets.Reset();

		if (TelemetryList.Len() > 0)
		{
			TelemetryList.ParseIntoArray(TelemetryArray, TEXT(","));

			for (FString& Telemetry : TelemetryArray)
			{
				void* Object = nullptr;
				FProperty* Property = UReflectionUtilsFunctionLibrary::RetrieveProperty(GetOwner(), Telemetry, Object);
				UFunction* Function = UReflectionUtilsFunctionLibrary::RetrieveFunction(GetOwner(), Telemetry, Object);

				FTelemetryTarget Target;
				Target.TargetObject = Object;
				Target.TargetProperty = Property;
				Target.TargetFunction = Function;

				TelemetryTargets.Add(Target);
			}
		}

		CurrentTelemetryList = TelemetryList;
	}
}

void UArenaCharacterTelemetryComponent::DrawTelemetry(UCanvas* Canvas, float& YL, float& YPos)
{
	static const float GraphW = 300.0f;
	//static const float GraphH = 150.0f;

	//static const int32 NumValues = 128;

	static const TArray<FColor> Colors = { FColor::Yellow, FColor::Red, FColor::Blue, FColor::Cyan, FColor::Magenta, FColor::Orange, FColor::Purple, FColor::White };
	static const int32 NumColors = 8;

	static const TArray<float> EmptyArray;

	static const float XPos = 4.f;
	static const float YSep = 4.f;

	static const FLinearColor BackgroundColor = FLinearColor(0.0f, 0.125f, 0.0f, 0.25f);
	static const FLinearColor NoColor = FLinearColor(0.0f, 0.f, 0.0f, 0.f);

	//FVector2D TelemetryRange;

	//TelemetryRange.X = CVarVehicleTelemetryRangeMin.GetValueOnGameThread();
	//TelemetryRange.Y = CVarVehicleTelemetryRangeMax.GetValueOnGameThread();

	UFont* SmallFont = GEngine->GetSmallFont();

	float CurrX = XPos;
	float CurrY = YPos + 16.f;

	// Read Console and parse command.

	TArray<float> TelemetryValues;

	for (FTelemetryTarget& Target : TelemetryTargets)
	{
		float Value = 0.f;

		if (Target.TargetObject && (Target.TargetProperty || Target.TargetFunction))
		{
			FProperty* Property = Target.TargetProperty;
			if (!Property) {
				uint8* Buffer = (uint8*)FMemory_Alloca(Target.TargetFunction->ParmsSize);
				UObject* TargetObject = (UObject*)Target.TargetObject;
				if (TargetObject) {
					TargetObject->ProcessEvent(Target.TargetFunction, Buffer);
					for (TFieldIterator<FProperty> PropIt(Target.TargetFunction, EFieldIteratorFlags::ExcludeSuper); PropIt; ++PropIt)
					{
						FProperty* InnerProperty = *PropIt;
						bool isOut = InnerProperty->HasAnyPropertyFlags(CPF_OutParm);
						if (isOut) {
							Property = InnerProperty;
							break;
						}
					}
					if (Property) {
						uint8* OutValueAddress = Property->ContainerPtrToValuePtr<uint8>(Buffer);
						float* FloatPtr = (float*)OutValueAddress;
						Value = *FloatPtr;
					}
				}
			}
			else {
				FFloatProperty* FloatProperty = CastField<FFloatProperty>(Property);
				if (FloatProperty)
				{
					Value = FloatProperty->GetPropertyValue_InContainer(Target.TargetObject);
				}
				else
				{
					FIntProperty* IntProperty = CastField<FIntProperty>(Property);
					if (IntProperty)
					{
						Value = IntProperty->GetPropertyValue_InContainer(Target.TargetObject);
					}
				}
			}
		}

		TelemetryValues.Add(Value);
	}

	// Draw title.

	{
		FString Label = "TELEMETRY";

		float OutX;
		LocDrawText(Canvas, Label, FColor::White, CurrX, CurrY, GraphW, OutX, CurrY);
	}

	CurrY += YSep;

	// Draw labels and values.

	int32 TelemetryIdx = 0;

	for (float Value : TelemetryValues)
	{
		FString TextToDraw = TelemetryArray[TelemetryIdx];
		TextToDraw = TextToDraw.Append(" : ");
		TextToDraw = TextToDraw.Append(FString::Printf(TEXT("%0.1f"), Value));

		FColor CurrentColor = Colors[TelemetryIdx % NumColors];

		float OutX, OutY;
		LocDrawText(Canvas, TextToDraw, CurrentColor, CurrX, CurrY, GraphW, OutX, OutY);

		CurrY = OutY;

		++TelemetryIdx;
	}

}

#pragma optimize("", on)