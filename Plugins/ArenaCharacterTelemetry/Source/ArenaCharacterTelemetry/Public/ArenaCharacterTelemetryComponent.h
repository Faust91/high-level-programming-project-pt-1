// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ArenaCharacterTelemetryComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class ARENACHARACTERTELEMETRY_API UArenaCharacterTelemetryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UArenaCharacterTelemetryComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void DrawTelemetry(UCanvas* Canvas, float& YL, float& YPos);

private:

	void UpdateTelemetryTargets();

	FString CurrentTelemetryList;
	TArray<FString> TelemetryArray;

	struct FTelemetryTarget {
		void* TargetObject;
		FProperty* TargetProperty;
		UFunction* TargetFunction;
	};

	TArray<FTelemetryTarget> TelemetryTargets;

};
